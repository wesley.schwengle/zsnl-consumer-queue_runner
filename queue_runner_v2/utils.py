# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import argparse
from datetime import datetime
from pythonjsonlogger import jsonlogger


class CustomJsonFormatter(jsonlogger.JsonFormatter):
    """Custom JSON log formatter, adds "zs_component" and "timestamp" fields.

    :param jsonlogger: extends class JsonFormatter from the jsonlogger module.
    :type jsonlogger: class
    """

    def add_fields(self, log_record, record, message_dict):
        """Normalize and dictate how all log messages are structured.

        :param log_record: log record
        :param record: record
        :param message_dict: message
        :type message_dict: dict
        """
        super(CustomJsonFormatter, self).add_fields(
            log_record, record, message_dict
        )

        log_record["zs_component"] = "queue-runner"

        created = datetime.fromtimestamp(record.created).strftime(
            "%Y-%m-%dT%H:%M:%S.%fZ"
        )
        log_record["timestamp"] = created

        log_record["level"] = record.levelname


def parse_arguments(arguments):
    """Parse argument list supplied argument parameter.

    :param arguments: argument list to parse
    :type arguments: list of arguments
    :return: dict with config values (defaults or specified)
    :rtype: dict
    """
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "--rabbitmq-url",
        default="localhost",
        help="Name amqp:// URL used to contact RabbitMQ server",
    )
    parser.add_argument(
        "--statsd-host", help="Hostname/IP of the StatsD server."
    )
    parser.add_argument(
        "--statsd-port", help="Port the StatsD server can be reached on."
    )
    parser.add_argument(
        "--debug",
        action="store_true",
        help="Run in debug mode, increasing verbosity.",
    )
    parser.add_argument(
        "--number-of-threads",
        default=10,
        help="Total number of threads that will be consuming incoming messages.",
    )
    parser.add_argument(
        "--heartbeats",
        default=60,
        help="Total number of threads that will be consuming incoming messages.",
    )
    return parser.parse_args(arguments)
