#!/bin/sh

# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

MQ_HOST="$1"
MQ_PORT="$2"
MQ_URL="$3"
STATSD_HOST="$4"
STATSD_PORT="$5"
NUM_THREADS="$6"

while ! nc -z "$MQ_HOST" "$MQ_PORT"; do
    echo "Waiting for RabbitMQ"
    sleep 1
done

if [ $NUM_THREADS ]
then
NUM_THREADS_ARG=--number-of-threads="$NUM_THREADS"
fi

python -m queue_runner_v2 --rabbitmq-url="$MQ_URL" --statsd-port="$STATSD_PORT" --statsd-host="$STATSD_HOST" $NUM_THREADS_ARG
